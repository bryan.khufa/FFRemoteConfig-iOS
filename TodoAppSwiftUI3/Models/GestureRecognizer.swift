//
//  GestureRecognizer.swift
//  TodoAppSwiftUI3
//
//  Created by Laura Caroline K on 12.07.21.
//

import Foundation
import UIKit

//hide keyboard on press outside
extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
