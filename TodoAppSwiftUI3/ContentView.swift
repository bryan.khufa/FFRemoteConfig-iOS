//
//  ContentView.swift
//  TodoAppSwiftUI3
//
//  Created by Laura Caroline K on 28.06.21.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject private var viewModel = ContentViewViewModel()
    
//    var todoTabActive = false
    
    var body: some View {
        if viewModel.todoTabActive {
            TabView {
                Menu()
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("Home")
                    }
                ToDoList()
                    .tabItem {
                        Image(systemName: "list.bullet.circle.fill")
                        Text("Todos")
                    }
            }
            .accentColor(Color.customAccentColor)
        } else {
            Menu()
                .tabItem {
                    Image(systemName: "house.fill")
                    Text("Home")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
