//
//  TodoAppSwiftUI3App.swift
//  TodoAppSwiftUI3
//
//  Created by Laura Caroline on 21.06.21.
//

import SwiftUI
import FirebaseCore
import FirebaseRemoteConfig
import FirebaseAnalytics

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                       didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        FirebaseApp.configure()

        
//        userID - random generate
//        userName - random generate
//        gender - male, female
//        subscribe - true, false
//        appVersion - build number app
//        os - android ios (ini hardcode aja, maksudnya di hardcode android, padahal os nya ios)
//        age - sepuh apa tidak sepuh

        
        Analytics.setUserID(getUserId())
        Analytics.setUserProperty("Chipmunk", forName: "userName")
        Analytics.setUserProperty("male", forName: "gender")
        Analytics.setUserProperty("true", forName: "subscribe")
        Analytics.setUserProperty("1.0.0", forName: "appVersion")
        Analytics.setUserProperty("iOS", forName: "os")
        Analytics.setUserProperty("23", forName: "age")
        
        RemoteConfig.remoteConfig().fetchAndActivate()
        return true
    }
    
    func getUserId() -> String {
        var userIds = [
            "ef6d07d4-3296-436d-a7e0-34db78a7e2f5",
            "68eb4894-a0ef-46f7-8b97-797183922772",
            "020f2a8d-10e3-496f-824d-373e8ee4fd86",
            "bf48eade-8974-45f8-8d0b-222ef450aca5",
            "ba54d272-588d-48f6-b25f-aa74edc6a36c",
            "b40484a1-2b99-47a6-a1ad-3123a55ff672",
            "0b6a6507-93dc-4483-aac2-604e1cc246ef",
            "bd99e002-228e-45dd-808f-2acb468ec9df",
            "0ea7e339-599e-4adf-83c5-a3e8fb9b25a3",
            "f3415076-4988-46fc-80d0-62bbc483743f",
        ]

        let randomIndex = Int.random(in: 0..<userIds.count)
        return userIds[randomIndex]
    }
}

@main
struct TodoAppSwiftUI3App: App {
    let persistenceController = PersistenceController.shared
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
            
        }
    }
}
