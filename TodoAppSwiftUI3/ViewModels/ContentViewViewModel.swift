//
//  ContentViewViewModel.swift
//  TodoAppSwiftUI3
//
//  Created by Rio Rizky Rainey Ferbiansyah on 24/08/22.
//

import Foundation
import FirebaseRemoteConfig

class ContentViewViewModel: ObservableObject {
    @Published var todoTabActive = true
    
    var remoteConfig = RemoteConfig.remoteConfig()
    let settings = RemoteConfigSettings()
    init() {
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
        
        remoteConfig.fetch { (status, error) -> Void in
          if status == .success {
              self.todoTabActive = self.remoteConfig["tab_todos"].boolValue
          }
        }
    }
}
