//
//  MainScreenViewModel.swift
//  TodoAppSwiftUI3
//
//  Created by Laura Caroline K on 23/08/22.
//

import Foundation
import FirebaseRemoteConfig
import SwiftUI

class MainScreenViewModel: ObservableObject {
    var homeTitle: String {
        updateTitle()
    }
    var fabIsIcon: Bool {
        remoteConfig["button_fab_icon"].boolValue
    }
    var categoriesIsList: Bool {
        remoteConfig["categories_list"].boolValue
    }
    
    var showPackage: Bool {
        remoteConfig["show_package"].boolValue
    }
    var showCategories: Bool {
        remoteConfig["show_categories"].boolValue
    }
    
    @Published var todoCellWidth: CGFloat = .infinity
    @Published var todoCellHeight: CGFloat = 100
    @Published var categoryCellWidth: CGFloat = .infinity
    @Published var categoryCellHeight: CGFloat = 190
    
    @AppStorage("userName") var userName = ""
    
    var remoteConfig = RemoteConfig.remoteConfig()
    let settings = RemoteConfigSettings()
    
    func fetchRemoteConfig() {
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
        remoteConfig.fetch { [weak self] (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self?.updateUI()
                self?.remoteConfig.activate { changed, error in
                    // ...
                }
                
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
        }
    }
    
    private func updateUI() {
        updateCellStyle()
    }
    
    private func updateTitle() -> String {
        let defaultTitle = remoteConfig["home_title"].stringValue ?? ""
        let greetings = remoteConfig["greetings_title"].stringValue ?? ""
        return userName.isEmpty ? defaultTitle : "\(greetings) \(userName)!"
    }
    
    private func updateCellStyle() {
        guard let cellStyleJson = remoteConfig["cell_style"].jsonValue as? NSDictionary,
              let todoStyle = cellStyleJson["todo"] as? NSDictionary,
              let categoryStyle = cellStyleJson["category"] as? NSDictionary
        else { return }
        
        todoCellWidth = todoStyle["width"] as? CGFloat ?? .infinity
        todoCellHeight = todoStyle["height"] as? CGFloat ?? 100
        
        categoryCellWidth = categoryStyle["width"] as? CGFloat ?? .infinity
        categoryCellHeight = categoryStyle["height"] as? CGFloat ?? 190
    }
}
